module gitlab.privy.id/achjailani/go-aesgcm

go 1.21.2

require (
	github.com/dozen/ruby-marshal v0.0.0-20220930161126-c9baae3c0e61
	golang.org/x/crypto v0.14.0
)
