package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"strings"

	rbmarshal "github.com/dozen/ruby-marshal"
	"golang.org/x/crypto/pbkdf2"
)

const SECRET_KEY = "3ae9b0ce19316f877554a0427044180e27267fb9798db9147feeb318865b3a52f79824201608f6e4e10dc8e3f29e5bf4b83e46c4103ff8d98b99903d054d720a"

// EncryptGCM
// reference on Rails 5.2-stable:
// https://github.com/rails/rails/blob/5-2-stable/activesupport/lib/active_support/message_encryptor.rb#L166C6-L166C6
func EncryptGCM(text string, secretKeyBase string) (string, error) {
	//	authTagSize := 16

	salt := make([]byte, 32)
	rand.Read(salt)

	saltKey := hex.EncodeToString(salt)

	key := GenerateKey(secretKeyBase, saltKey)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", fmt.Errorf(`err aesNewCipher got %v`, err)
	}

	aesGCM, err := cipher.NewGCM(block)

	aesGCM.NonceSize()
	if err != nil {
		return "", fmt.Errorf(`err chipperNewGCM got %v`, err)
	}

	iv := make([]byte, aesGCM.NonceSize())
	rand.Read(iv)

	/*
		ciphertext := aesGCM.Seal(nil, iv, []byte(text), nil)
		textEncode := base64.StdEncoding.EncodeToString(ciphertext)
		ivEncode := base64.StdEncoding.EncodeToString(iv)

		authTagEncode := base64.StdEncoding.EncodeToString(ciphertext[len(ciphertext)-authTagSize:])
	*/
	w := bytes.NewBuffer([]byte{})
	rbmarshal.NewEncoder(w).Encode(&text) // Fix 1
	textSerialized := w.Bytes()

	ciphertextTag := aesGCM.Seal(nil, iv, textSerialized, nil)
	border := len(ciphertextTag) - 16
	ciphertext := ciphertextTag[:border] // Fix 2
	authTag := ciphertextTag[border:]

	textEncode := base64.StdEncoding.EncodeToString(ciphertext)
	ivEncode := base64.StdEncoding.EncodeToString(iv)
	authTagEncode := base64.StdEncoding.EncodeToString(authTag)

	return fmt.Sprintf("%s$$%s--%s--%s", saltKey, textEncode, ivEncode, authTagEncode), nil
}

// DecryptGCM
// reference on Rails 5.2-stable:
// https://github.com/rails/rails/blob/5-2-stable/activesupport/lib/active_support/message_encryptor.rb#L183
func DecryptGCM(encryptedText string, secretKeyBase string) (string, error) {
	encryptText := strings.Split(encryptedText, "$$")
	saltHex := encryptText[0]
	encodedText := encryptText[1]

	splitEncodedText := strings.Split(encodedText, "--")
	encodedText = splitEncodedText[0]
	ivText := splitEncodedText[1]
	authTagText := splitEncodedText[2]

	decodeText, err := base64.StdEncoding.DecodeString(encodedText)
	if err != nil {
		return "", fmt.Errorf(`err b64 decode text got %v`, err)
	}

	ivDecodeText, err := base64.StdEncoding.DecodeString(ivText)
	if err != nil {
		return "", fmt.Errorf(`err b64 iv got %v`, err)
	}

	authTagTextDecoded, err := base64.StdEncoding.DecodeString(authTagText)
	if err != nil {
		return "", fmt.Errorf(`err b64 auth tag got %v`, err)
	}

	key := GenerateKey(secretKeyBase, saltHex)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", fmt.Errorf(`err aesNewCipher got %v`, err)
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", fmt.Errorf(`err chipperNewGCM got %v`, err)
	}

	plaintext, err := aesGCM.Open(nil, ivDecodeText, append(decodeText, authTagTextDecoded...), nil) // Fix 1
	if err != nil {
		return "", fmt.Errorf(`err aesGCMOpen got %v`, err)
	}

	var v string
	rbmarshal.NewDecoder(bytes.NewReader(plaintext)).Decode(&v) // Fix 2
	return string(v), nil
}

func GenerateKey(secretKeyBase string, saltHex string) []byte {
	key := pbkdf2.Key([]byte(secretKeyBase), []byte(saltHex), 65536, 32, sha1.New)
	return key
}

func main() {

	text := "Hello, world"
	e, err := EncryptGCM(text, SECRET_KEY)
	if err != nil {
		fmt.Println("Error encrypt: ", err)
	}
	fmt.Println("Encrypted")
	fmt.Println(e)

	d, err := DecryptGCM(e, SECRET_KEY)
	if err != nil {
		fmt.Println("Error decrypt: ", err)

	}
	fmt.Println("Decrypted")
	fmt.Println(d)

	// Value Decrypt = d5cbc0af8b124ef65511f3e56fa9c0ba7b9b4e7612a4f755e92c52ffc3eeaa83-37bd3d7c-81f5-4fad-a5db-13a10e1253eb
	encryptText := "fb24a24743685ada243dade4b9960944f3acf3a7c3530b47893531f26889cb68$$elTCuPPgeIGe6rbq9zeL2ypgLGEBK/X42V4tgTGCrrkT0C5DekUZn84AHPIPcsEHFdA7jDds7aJlRH8kxwwidwuNstYYEd8Xaph95lVZtXLZ7q1z7locIO9DxLWuCZ/pLQtFOwrcAwhxlunLT+xL--qz/NFMn8iHP41MCD--QjBV+VT2ufAWHcuQFGs+QA=="

	decryptRailsToGolang, err := DecryptGCM(encryptText, SECRET_KEY)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(`result_decrypt_rails_to_golang`, decryptRailsToGolang)
}
